#!/usr/bin/ruby

########## COLOR OUTPUT Functions
def blue    (str, tg=:fg)  if tg == :bg then return "\e[44m#{str}\e[49m" else return "\e[34m#{str}\e[0m" end end
def red     (str, tg=:fg)  if tg == :bg then return "\e[41m#{str}\e[49m" else return "\e[31m#{str}\e[0m" end end
def green   (str, tg=:fg)  if tg == :bg then return "\e[42m#{str}\e[49m" else return "\e[32m#{str}\e[0m" end end
def yellow  (str, tg=:fg)  if tg == :bg then return "\e[43m#{str}\e[49m" else return "\e[33m#{str}\e[0m" end end

def yellowbg(str) return yellow(str,:bg) end

def b(str) return "\e[1m#{str}\e[22m" end
def i(str) return "\e[3m#{str}\e[23m" end
  
#####################
class Service
  attr_accessor :name
  
  @@start = Hash.new
  @@stop = Hash.new
  
  #################### Define here the shell commands
  @@start[:dummy] = "echo \"starting test succed !\""
  @@stop[:dummy]  = "echo \"stopping test succed !\""
  
  @@start[:postgres] = "pg_ctl -D /usr/local/var/postgres -l /usr/local/var/postgres/server.log start"
  @@stop[:postgres]  = "pg_ctl -D /usr/local/var/postgres stop -s -m fast"
  ####################
  
  def initialize name
    @name = name
  end
  
  def start
    %x[#{@@start[@name.to_sym]}]
  end
  
  def stop
    %x[#{@@stop[@name.to_sym]}]
  end
  
  def restart
    self.start
    self.stop
  end
  
end

#####################

def run(command, service)
  serv = Service.new service
  
  case command
  when "start"
    puts yellow "Starting #{serv.name}..."
    x = serv.start
  when "stop"
    puts yellow "Stopping #{serv.name}..."
    x = serv.stop
  when "restart"
    puts yellow "Restarting #{serv.name}..."
    x = serv.restart
  else
    puts yellowbg red "Available commands are : start, stop, restart."
  end

  if x
    puts green "Done!"
  else
    puts red "Error :-("
  end
end

# Syntax : ""./service.rb start A B C" => starts A, B and C
ARGV.drop(1).each {|a| run(ARGV[0], a) }




