#!/usr/bin/ruby

je_suis_un_connard_pingre_et_degenere_et_je_l_assume_pleinement = true

dl_to = "~/Music/"
simple = true
verbose = false

require 'optparse'

########## COLOR OUTPUT Functions
def blue    (str, tg=:fg)  if tg == :bg then return "\e[44m#{str}\e[49m" else return "\e[34m#{str}\e[0m" end end
def red     (str, tg=:fg)  if tg == :bg then return "\e[41m#{str}\e[49m" else return "\e[31m#{str}\e[0m" end end
def green   (str, tg=:fg)  if tg == :bg then return "\e[42m#{str}\e[49m" else return "\e[32m#{str}\e[0m" end end
def yellow  (str, tg=:fg)  if tg == :bg then return "\e[43m#{str}\e[49m" else return "\e[33m#{str}\e[0m" end end

def yellowbg(str) return yellow(str,:bg) end

def b(str) return "\e[1m#{str}\e[22m" end
def i(str) return "\e[3m#{str}\e[23m" end

# Gnéhéhé
if je_suis_un_connard_pingre_et_degenere_et_je_l_assume_pleinement then puts b i red "Tu sais que c'est vilain de télécharger ?\nTu te prends pour une pir4t3, hein ?\nPetite turlupine, va !" end
  
# On demande l'URL du machin à DDL
puts blue yellowbg "Quelle est l'adresse du bidule à télécharger ?"
url = gets.chomp

command = ""
unless simple
  puts yellow "Je recherche < #{url} >..." # Résultat youtube-dl
  raw = %x{youtube-dl -F #{url}} # Affichage
  res = raw.split("note").last.split("\n")
  #puts res
  i = -1;
  res.each do |line|
    i += 1
    if i < 1 then next end # skip first empty line
    puts line
  end

  puts blue yellowbg "Quel est le numéro du format désiré ?"
  num = gets.chomp
  
  command = "#{num}"
else
  puts blue yellowbg "Audio seulement (appuyez sur Entrée) ou Vidéo (tapez V et appuyez sur Entrée) ?"
  res = gets.chomp
  if res.downcase == "v"
    command = "bestvideo"
  else
    command = "bestaudio"
  end
end

puts yellow "D'accord ! Je télécharge ça...\nCela va prendre un petit moment, veuillez patienter."

# Commande de téléchargement
res = %x{youtube-dl -o "#{dl_to}%(title)s.%(ext)s" -f #{command} #{url}}

if verbose then puts green "Fichier téléchargé dans : #{dl_to}" end
puts b i green "Et voilà !"

# Infos : combien a été téléchargé, en combien de temps
if verbose
  res = res.split("[download]").last
  res = res.split("has been").first
  puts b i green "#{res}"
end

# Gnéhéhé (bis)
if je_suis_un_connard_pingre_et_degenere_et_je_l_assume_pleinement then puts b i red "HADOPI AURA TA PEAU !!" end